{:user
 {:plugins [[cider/cider-nrepl "0.18.0"]
            [lein-pprint "1.1.1"]
            [refactor-nrepl "2.4.0"]
            [venantius/ultra "0.5.2"]
            ]
  :dependencies [[org.clojure/tools.nrepl "0.2.12"]]}}
