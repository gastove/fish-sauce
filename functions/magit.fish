function magit -d "Open magit-status in the terminal, in Emacs"
    emacs-invoke -t '(magit-status)'
end
