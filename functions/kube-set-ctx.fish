function kube-set-ctx
    kubectl config use-context $argv
    if test $status -ne 0
        echo Failed to change context
    end
end
