set _gastove_brew_packages \
'aspell --with-lang=en' \
'global --with-exuberant-ctags' \
bash \
coreutils \
ctags \
editorconfig \
forge \
freetype \
fzf \
gcc \
git \
htop \
imagemagick \
isync \
jpeg \
jq \
leiningen \
maven \
mono \
npm \
optipng \
pandoc \
python \
rg \
rsync \
sbt \
scala \
the_silver_searcher \
tree \
wget \
zmq

function _brew_package_installed
    set -l parts (string split ' ' $argv)
    echo 'checking for' $parts[1]
    brew ls --versions $parts[1] > /dev/null
end


function _assure_brew_emacs
    if not _brew_package_installed emacs-mac
        brew tap railwaycat/emacsmacport
        brew install emacs-mac --with-imagemagick --with-modern-icon --with-gnutls
    end
end

function assure_brew_packages
    # assure_brew_emacs
    for package in $_gastove_brew_packages
        if not _brew_package_installed $package
            brew install package
        else
            echo $package already installed...
        end
    end
end
