function acquire -d "Clone a git project and add it to Emacs Projectile"
    set -l options "o/open"
    argparse -n acquire $options -- $argv
    or return

    set -l cur_dir (pwd)
    set -l project $argv[1]
    set -l project_dir (basename -s .git $project)

    git clone $project

    cd $project_dir
    emacs-add-project

    if set -q _flag_open
        eval $EDITOR .
    else
        cd ..
    end
end
