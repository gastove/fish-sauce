function goto -d "Go to a known Emacs project's source directory"
    set -l project (emacs-invoke "(orary/list-projects)" | string trim -c \" | string split ' ' | fzf)
    cd (string replace '~/' "$HOME/" $project)
end
