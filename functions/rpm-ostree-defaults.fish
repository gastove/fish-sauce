set _gastove_rpm_ostree_packages \
'bat' \
'byobu' \
'exa' \
'fd-find' \
'fontawesome-fonts' \
'fzf' \
'gammastep' \
'gammastep-indicator' \
'GraphicsMagick' \
'htop' \
'jq' \
'light' \
'mako' \
'net-tools' \
'network-manager-applet' \
'pandoc' \
'ripgrep' \
'rofi' \
'slurp' \
'solaar' \
'sway' \
'swayidle' \
'swaylock' \
'tokei' \
'waybar' \
'wl-clipboard' \
'xclip' \
'xdg-desktop-portal-wlr'

function rpm-ostree-defaults
    rpm-ostree install --idempotent $_gastove_rpm_ostree_packages
end
