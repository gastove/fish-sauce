function kube-run-debug-pod
    kubectl --namespace $argv[1] run \
        --generator=run-pod/v1 -i --tty=true \
        --requests='cpu=100m,memory=100Mi' \
        --limits='cpu=100m,memory=100Mi' \
        rmd-debug \
        --image=registry.gitlab.com/gastove/containers/toolbox/networking:latest -- bash
end
