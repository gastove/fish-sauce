set _gastove_golang_binaries \
"github.com/go-delve/delve/cmd/dlv" \
"honnef.co/go/tools/cmd/staticcheck" \
"golang.org/x/tools/cmd/goimports"

function gotools -d "Install or update assorted useful Golang development tools"
    # We CD to /tmp before execution because `go get` in a module adds the
    # package in question as a dep.
    set -l original_dir (pwd)
    cd /tmp
    for package in $_gastove_golang_binaries
        go get -v -u $package
    end

    # If you install gopls with -u everything detonates
    go get -v golang.org/x/tools/gopls@latest

    cd $original_dir
end
