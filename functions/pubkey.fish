function pubkey
    gpg2 --export-ssh-key $__gpg_key_hash | wl-copy
    echo "Public key copied to clipboard ✅"
end
