function bin-link
    mkdir -p "$HOME/bin"
    ln -s $HOME/.config/fish/bin/* $HOME/bin/
end
