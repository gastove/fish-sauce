set _gastove_dotfiles_src_dir ~/.config/fish/dotfiles

function _link_dotfile
    set -l _src_dotfile $_gastove_dotfiles_src_dir/$argv[1]
    set -l _dest_dotfile ( string join "/" $argv[2] $argv[3])

    mkdir -p $argv[2]

    # If a given file already exists, we're set; otherwise, symlink
    if not test -e "$_dest_dotfile"
        ln -s $_src_dotfile $_dest_dotfile
        set_color $__rhombus_green
        echo Linked $argv[1] to $_dest_dotfile
        set_color normal
    else
        echo Dotfile already exists: $_dest_dotfile
    end
end

function dotfiles
    echo Linking all dotfiles...
    # Links all go here
    # This is so out of date :|
    # _link_dotfile profiles.clj ~/ .lein
    _link_dotfile gpg-agent.conf $HOME .gnupg
    _link_dotfile .gitconfig ~/ .gitconfig
    _link_dotfile alacritty.toml ~/.config/alacritty alacritty.toml
    _link_dotfile bottom.toml ~/.config/bottom bottom.toml
    _link_dotfile sway.conf ~/.config/sway config
    _link_dotfile waybar.json ~/.config/waybar config
    _link_dotfile waybar-style.css ~/.config/waybar style.css
    _link_dotfile rofi.conf ~/.config/rofi config
    _link_dotfile mako.conf ~/.config/mako config
    _link_dotfile wofi.conf ~/.config/wofi config
    _link_dotfile wofi.css ~/.config/wofi style.css
    _link_dotfile gammastep.ini ~/.config/gammastep config.ini
    _link_dotfile starship.toml ~/.config starship.toml
    _link_dotfile windows.tmux ~/.byobu windows.tmux
    _link_dotfile tmux.conf ~/.byobu tmux.conf
    # End of links
    echo -n Done
    set_color $__rhombus_green
    echo " ✔"
    set_color normal
end
