function emacs-add-project -d "Adds a project to Projectile in Emacs"
    set -l curdir (pwd)
    set -l adddir (pwd)

    if count $argv > /dev/null
        set adddir $argv[1]
    end

    cd $adddir

    emacs-invoke "(orary/projectile-add-cwd)" > /dev/null
    if test $status -eq 0
        echo 👍 (pwd) is a known project
    else
        echo 👎 Failed to add (pwd) to known projects
    end

    cd $curdir
end
