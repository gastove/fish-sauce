function systemd-link
    set -l unitdir $HOME/.config/systemd/user/
    mkdir -p $unitdir
    for unit in (find $fish_home/systemd/ -type f)
        ln -s $unit $unitdir
        set_color $__rhombus_green
        echo Linked $unit to $unitdir
        set_color normal
    end
end
