function pip-install-devtools
    pip install -U pip
    pip install -U black jedi flake8 mypy
end
