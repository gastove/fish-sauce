set _gastove_fedora_packages \
aspell \
aspell-en \
autoconf \
automake \
byobu \
chrome-gnome-shell \
copyq \
emacs \
fzf \
gcc \
gcc-c++ \
gcc-gfortran \
git \
global \
gmime-devel \
golang \
htop \
iotop \
libcurl-devel \
libX11-devel \
libXt-devel \
lldb \
openssl-devel \
pandoc \
\'readline\' \
readline-devel \
ripgrep \
sysstat \
texlive \
the_silver_searcher \
xapian-core-devel \
xclip \
xz-devel

function assure_dnf_packages
    sudo dnf upgrade -y
    sudo dnf install -y $_gastove_fedora_packages
end
