function emacs-invoke -d "Calls its argument as an Emacs Lisp function"
    set -l options "t/terminal" "c/client"
    argparse -n acquire $options -- $argv
    or return

    set -l binary ""
    if set -q _flag_terminal
        set binary $_editor"client -t"
    else if set -q _flag_client
        set binary $_editor"client -c"
    else
        set binary $_editor"client"
    end

    set -l invocation "$binary -e \"$argv[1]\""
    eval $invocation
end
