function assure_on_path
    if not contains $argv[1] $PATH
        set PATH $argv[1] $PATH
    end
end
