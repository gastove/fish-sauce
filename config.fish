set -g fish_home $HOME/.config/fish
# Configure fisher to store its functions in a safely gitignored location;
# update the path so everything can be found.
set -g fisher_path $HOME/.config/fish/fisher

# Fish Function Path for Fisher
set fish_function_path $fish_function_path[1] $fisher_path/functions $fish_function_path[2..-1]
# Fish Function Path for local functions
set fish_function_path -a $fish_home/functions/local $fish_function_path
set fish_complete_path $fish_complete_path[1] $fisher_path/completions $fish_complete_path[2..-1]

for file in $fisher_path/conf.d/*.fish
    builtin source $file 2> /dev/null
end

# Bootstrapping
if not functions -q fisher
    set -q XDG_CONFIG_HOME; or set XDG_CONFIG_HOME ~/.config
    curl https://git.io/fisher --create-dirs -sLo $fisher_path/functions/fisher.fish
    fish -c fisher
    assure_dnf_packages
    assure_go_binaries
end

# virtualfish does not appear to be able to find these functions until they're
# explicitly sourced, so -- we lump them into a single file, then source them
# all together. Cool.
for file in $fish_home/functions/local/venv*.fish
    builtin source $file ^ /dev/null
end

# SSH Auth via Yubikey
# Hilarious but true: after getting everything wired up for this...
# SquareSpace does something totally different and incompatible.
# Cool.
set -x GPG_TTY (tty)
set -x SSH_AUTH_SOCK (gpgconf --list-dirs agent-ssh-socket)

# The PATH
assure_on_path $HOME/bin
assure_on_path $HOME/.cargo/bin
assure_on_path $HOME/.local/bin

# Local configs (might also modify PATH)
if test -e ~/.local.fish
    source ~/.local.fish
end

# Get zoxide setup
zoxide init fish | source

# Last step in path modification, virtualfish:
set VIRTUALFISH_HOME "$HOME/.python_virtualenvs"

# Various config vars
set -U _editor emacs # Due it instability in Remacs I wind up going back and forth, ugh.
set -gx EDITOR emacsclient -t
set -x WORKON_HOME $VIRTUALFISH_HOME

# Aliases I like to have everywhere
alias gpg gpg2
alias docker 'sudo docker'
alias el 'eza -la'
alias et 'eza -T'
alias eta 'eza -Ta'
alias etl 'eza -Ta -L'

alias ec $_editor'client -c'
alias ect $_editor'client -t'

alias k kubectl

alias j 'bat -l json'
alias y 'bat -l yaml'
alias less bat
alias hf hyperfine

alias bctl bluetoothctl

alias sysd systemctl
alias sysdu 'systemctl --user'

alias te 'toolbox enter -c'
alias tem 'toolbox enter -c dev-main'

alias pbcopy wl-copy
alias pbpaste wl-paste

# My very own colors <3
# Rhombus Theme ヽ(⌐■_■)ノ♪♬
# Make my colors available as defaults
set -U __rhombus_fg FFF5EB

set -U __rhombus_dark_red 9e1200
set -U __rhombus_dull_red 9e4d4a
set -U __rhombus_red Ff2600
set -U __rhombus_orange fa9a4b
set -U __rhombus_dull_orange 996011
set -U __rhombus_bronze D79600

set -U __rhombus_yellow D6d300
set -U __rhombus_berry 3D002E
set -U __rhombus_purple 932092
set -U __rhombus_violet A630db
set -U __rhombus_red_violet 9e3564
set -U __rhombus_pink D82e88
set -U __rhombus_off_white FFE0B2
set -U __rhombus_charcoal 656868
set -U __rhombus_grey Bbbbbb
set -U __rhombus_dark_brown 9d5717
set -U __rhombus_light_brown Fff59f

set -U __rhombus_dark_blue 483d8b
set -U __rhombus_green 006400
set -U __rhombus_blue 6495ed
set -U __rhombus_teal 65A399
set -U __rhombus_sand C7B299

set -U __rhombus_link 8ACDAA
set -U __rhombus_warn Ff2600
set -U __rhombus_succ 9d5717
set -U __rhombus_hl 1D1D1D

# Tell fish to use my colors
set fish_color_normal $__rhombus_fg
set fish_color_operator $__rhombus_orange

direnv hook fish | source
starship init fish | source
status --is-login; and status --is-interactive; and exec byobu-launcher
# NOTE[rdonaldson|2023-02-10] I don't know what envman is. 
# Generated for envman. Do not edit.
# test -s "$HOME/.config/envman/load.fish"; and source "$HOME/.config/envman/load.fish"

